{
'dateTimeFormat': 'iso8601',

'events' : [
        {'start': '1941',
        'title': 'Konrad Zuse creates the Z3 computer',
        'description': 'Konrad Zuse finishes the Z3 computer. The Z3 was an early computer built by German engineer Konrad Zuse working in complete isolation from developments elsewhere. Using 2,300 relays, the Z3 used floating point binary arithmetic and had a 22-bit word length. The original Z3 was destroyed in a bombing raid of Berlin in late 1943. However, Zuse later supervised a reconstruction of the Z3 in the 1960s which is currently on display at the Deutsches Museum in Munich.',
		'caption': 'Konrad Zuse finishes the Z3 computer. The Z3 was an early computer built by German engineer Konrad Zuse working in complete isolation from developments elsewhere. Using 2,300 relays, the Z3 used floating point binary arithmetic and had a 22-bit word length. The original Z3 was destroyed in a bombing raid of Berlin in late 1943. However, Zuse later supervised a reconstruction of the Z3 in the 1960s which is currently on display at the Deutsches Museum in Munich.',
        'image': 'http://www.computerhistory.org/timeline/images/1941_zuse_z3.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1941'
        },


        {'start': '1944',
        'title': 'Howard Aiken completes the Harvard Mark 1 computer',
        'description': 'Harvard Mark-1 is completed. Conceived by Harvard professor Howard Aiken, and designed and built by IBM, the Harvard Mark-1 was a room-sized, relay-based calculator. The machine had a fifty-foot long camshaft that synchronized the machine�s thousands of component parts. The Mark-1 was used to produce mathematical tables but was soon superseded by stored program computers.',
        'image': 'http://www.computerhistory.org/timeline/images/1944_harvard_markI.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1944'
        },

        {'start': '1944',
        'title': 'Colossus vacuum tube computer is completed at Bletchley Park in the UK',
        'description': 'The first Colossus is operational at Bletchley Park. Designed by British engineer Tommy Flowers, the Colossus was designed to break the complex Lorenz ciphers used by the Nazis during WWII. A total of ten Colossi were delivered to Bletchley, each using 1,500 vacuum tubes and a series of pulleys transported continuous rolls of punched paper tape containing possible solutions to a particular code. Colossus reduced the time to break Lorenz messages from weeks to hours. The machine�s existence was not made public until the 1970s',
        'image': 'http://www.computerhistory.org/timeline/images/1944_Colossus.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1944'
        },
		
		{'start': '1945',
        'title': 'Grace Hopper finds a moth in the Harvard Mark II -- the first computer bug.',
        'description': 'On September 9th, Grace Hopper recorded the first actual computer "bug" � a moth stuck between the relays and logged at 15:45 hours on the Harvard Mark II. Hopper, a rear admiral in the U.S. Navy, enjoyed successful careers in academia, business, and the military while making history in the computer field. She helped program the Harvard Mark I and II and developed the first compiler, A-0. Her subsequent work on programming languages led to COBOL, a language specified to operate on machines of different manufacturers.',
        'image': 'http://www.computerhistory.org/timeline/images/1945_hopper.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1945'
        },
		
		{'start': '1945',
        'title': 'Konrad Zuse began writing the Plankalkul programming language'
        },
		
		{'start': '1943',
		'end': '1946',
        'title': 'ENIAC digital computer is completed ',
        'description': 'In February, the public got its first glimpse of the ENIAC, a machine built by John Mauchly and J. Presper Eckert that improved by 1,000 times on the speed of its contemporaries.',
        'image': 'http://www.computerhistory.org/timeline/images/1946_eniac.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1946'
        },
		
		{'start': '1947',
        'title': 'William Shockley creates the first transistor.',
        'description': 'On December 23, William Shockley, Walter Brattain, and John Bardeen successfully tested this point-contact transistor, setting off the semiconductor revolution. Improved models of the transistor, developed at AT&T Bell Laboratories, supplanted vacuum tubes used on computers at the time.',
        'image': 'http://www.computerhistory.org/timeline/images/1947_point.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1947'
        },
		
		{'start': '1952',
        'title': 'Nixdorf computer company founded in Germany. ',
        'description': 'Heinz Nixdorf founded Nixdorf Computer Corp. in Germany. It remained an independent corporation until merging with Siemens in 1990.',
        'link': 'http://www.computerhistory.org/timeline/?year=1952'
        },
		
		{'start': '1952',
        'title': 'Grace Hooper writes the first compiler (A-0)',
        'description': 'Grace Hopper completes the A-0 Compiler. In 1952, mathematician Grace Hopper completed what is considered to be the first compiler, a program that allows a computer user to use English-like words instead of numbers. Other compilers based on A-0 followed: ARITH-MATIC, MATH-MATIC and FLOW-MATIC [software]',
        'image': 'http://www.computerhistory.org/timeline/images/1952_hopper-grace.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1952'
        },
		{'start': '1952',
        'title': 'IBM 726 implements high speed magnetic tape storage',
        'description': 'Magnetic tape allows for inexpensive mass storage of information and so is a key part of the computer revolution.  The IBM 726 was one of the first practical high-speed magnetic tape systems for electronic digital computers.  Announced on May 21, 1952, the system used a unique �vacuum channel� method of keeping a loop of tape circulating between two points allowing the tape drive to start and stop the tape in a split-second.  The Model 726 was first sold with IBM�s first electronic digital computer the Model 701 and could store 2 million digits per tape�an enormous amount at the time.  It rented for $850 a month.',
        'image': 'http://www.computerhistory.org/timeline/images/1952_ibm726.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1952'
        },		
		{'start': '1956',
        'title': 'IBM magnetic disk becomes available.',
        'description': 'The era of magnetic disk storage dawned with IBM�s shipment of a 305 RAMAC to Zellerbach Paper in San Francisco. The IBM 350 disk file served as the storage component for the Random Access Method of Accounting and Control. It consisted of 50 magnetically coated metal platters with 5 million bytes of data. The platters, stacked one on top of the other, rotated with a common drive shaft.',
        'image': 'http://www.computerhistory.org/timeline/images/1956_ramac_rjohnson.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1956'
        },	
		{'start': '1957',
        'title': 'FORTRAN language becomes available',
        'description': 'A new language, FORTRAN (short for FORmula TRANslator), enabled a computer to perform a repetitive task from a single set of instructions by using loops. The first commercial FORTRAN program ran at Westinghouse, producing a missing comma diagnostic. A successful attempt followed.',
        'image': 'http://www.computerhistory.org/timeline/images/1957_fortran.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1957'
        },	
		{'start': '1960',
        'title': 'COBOL language becomes available. Also LISP. ',
        'description': 'A team drawn from several computer manufacturers and the Pentagon developed COBOL, Common Business Oriented Language. Designed for business use, early COBOL efforts aimed for easy readability of computer programs and as much machine independence as possible. Designers hoped a COBOL program would run on any computer for which a compiler existed with only minimal modifications.<br >Howard Bromberg, an impatient member of the committee in charge of creating COBOL, had this tombstone made out of fear that the language had no future. However, COBOL has survived to this day.<br >LISP made its debut as the first computer language designed for writing artificial intelligence programs. Created by John McCarthy, LISP offered programmers flexibility in organization.',
		'caption': 'A team drawn from several computer manufacturers and the Pentagon developed COBOL, Common Business Oriented Language. Designed for business use, early COBOL efforts aimed for easy readability of computer programs and as much machine independence as possible. Designers hoped a COBOL program would run on any computer for which a compiler existed with only minimal modifications.<br >Howard Bromberg, an impatient member of the committee in charge of creating COBOL, had this tombstone made out of fear that the language had no future. However, COBOL has survived to this day.',
        'image': 'http://www.computerhistory.org/timeline/images/1960_cobol.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1960'
        },			
		{'start': '1960',
        'title': 'ESC key is invented by Bob Bemer at IBM',
        'description': 'The key was born in 1960, when an I.B.M. programmer named Bob Bemer was trying to solve a Tower of Babel problem: computers from different manufacturers communicated in a variety of codes. Bemer invented the ESC key as way for programmers to switch from one kind of code to another. ',
		'caption': 'The key was born in 1960, when an I.B.M. programmer named Bob Bemer was trying to solve a Tower of Babel problem: computers from different manufacturers communicated in a variety of codes. Bemer invented the ESC key as way for programmers to switch from one kind of code to another. ',
        'image': 'http://graphics8.nytimes.com/images/2012/10/07/magazine/07wmt/07wmt-articleLarge.jpg',
        'link': 'http://www.nytimes.com/2012/10/07/magazine/who-made-that-escape-key.html'
        },	

		{'start': '1962',
        'title': 'J.C.R. Licklider becomes head of the computer research program at ARPA (Advanced Research Projects Agency). The world has 10,000 computers.',
        'description': 'J.C.R. Licklider writes memos about his Intergalactic Network concept, where everyone on the globe is interconnected and can access programs and data at any site from anywhere. He is talking to his own �Intergalactic Network� of researchers across the country. In October, �Lick� becomes the first head of the computer research program at ARPA, which he calls the Information Processing Techniques Office (IPTO).',
		'caption': 'J.C.R. Licklider writes memos about his Intergalactic Network concept, where everyone on the globe is interconnected and can access programs and data at any site from anywhere. He is talking to his own �Intergalactic Network� of researchers across the country. In October, �Lick� becomes the first head of the computer research program at ARPA, which he calls the Information Processing Techniques Office (IPTO).',
        'image': 'http://www.computerhistory.org/internet_history/internet_poster.gif',
        'link': 'http://www.computerhistory.org/internet_history/'
        },
		{'start': '1962',
        'title': 'First interactive computer game at MIT �Spacewar!�.',
        'description': 'MIT students Slug Russell, Shag Graetz, and Alan Kotok wrote SpaceWar!, considered the first interactive computer game. First played at MIT on DEC�s PDP-1, the large-scope display featured interactive, shoot�em-up graphics that inspired future video games. Dueling players fired at each other�s spaceships and used early versions of joysticks to manipulate away from the central gravitational force of a sun as well as from the enemy ship.',
		'caption': 'MIT students Slug Russell, Shag Graetz, and Alan Kotok wrote SpaceWar!, considered the first interactive computer game. First played at MIT on DEC�s PDP-1, the large-scope display featured interactive, shoot�em-up graphics that inspired future video games. Dueling players fired at each other�s spaceships and used early versions of joysticks to manipulate away from the central gravitational force of a sun as well as from the enemy ship.',
        'image': 'http://www.computerhistory.org/timeline/images/1962_spacewar.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1962'
        },
		{'start': '1962',
        'title': 'ICPSR (Interuniversity Consortium for Political and Social Research) founded',
        'description': 'The Inter-university Consortium for Political Research (ICPR, as it was first known) was created in 1962 by political scientist Warren E. Miller as the data dissemination arm of the American National Election Studies (ANESi). ',
		'caption': 'The Inter-university Consortium for Political Research (ICPR, as it was first known) was created in 1962 by political scientist Warren E. Miller as the data dissemination arm of the American National Election Studies (ANESi). ',
        'image': 'http://www.icpsr.umich.edu/files/fifty/images/50-logo.jpg',
        'link': 'http://www.icpsr.umich.edu/icpsrweb/fifty/index.jsp'
        },				
		{'start': '1963',
        'title': 'ASCII (American Standard Code for Information Exchange) is defined.',
        'description': 'A joint industry-government committee develops ASCII (American Standard Code for Information Interchange), the first universal standard for computers. It permits machines from different manufacturers to exchange data. 128 unique 7-bit strings stand for either a letter of the English alphabet, one of the Arabic numerals, one of an assortment of punctuation marks and symbols, or a special function, such as the carriage return.',
		'caption': 'A joint industry-government committee develops ASCII (American Standard Code for Information Interchange), the first universal standard for computers. It permits machines from different manufacturers to exchange data. 128 unique 7-bit strings stand for either a letter of the English alphabet, one of the Arabic numerals, one of an assortment of punctuation marks and symbols, or a special function, such as the carriage return.',
        'image': 'http://www.computerhistory.org/internet_history/thumbs/ascii_t.gif',
        'link': 'http://www.computerhistory.org/internet_history/'
        },
		{'start': '1964',
        'title': 'IBM announces its System 360.',
        'description': 'IBM announced the System/360, a family of six mutually compatible computers and 40 peripherals that could work together. The initial investment of $5 billion was quickly returned as orders for the system climbed to 1,000 per month within two years. At the time IBM released the System/360, the company was making a transition from discrete transistors to integrated circuits, and its major source of revenue moved from punched-card equipment to electronic computer systems.',
		'caption': 'IBM announced the System/360, a family of six mutually compatible computers and 40 peripherals that could work together. The initial investment of $5 billion was quickly returned as orders for the system climbed to 1,000 per month within two years. At the time IBM released the System/360, the company was making a transition from discrete transistors to integrated circuits, and its major source of revenue moved from punched-card equipment to electronic computer systems.',
        'image': 'http://www.computerhistory.org/timeline/images/1964_ibm360.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1964'
        },	
		{'start': '1967',
        'title': 'OCLC begins operation as the Ohio College Library Consortium ',
        'description': 'In 1967, a small group of library leaders believed that working together, they could find solutions to the day\'s most pressing issues facing libraries. They began with the idea of combining computer technology with library cooperation to reduce costs and improve services through shared, online cataloging.',
		'caption': 'In 1967, a small group of library leaders believed that working together, they could find solutions to the day\'s most pressing issues facing libraries. They began with the idea of combining computer technology with library cooperation to reduce costs and improve services through shared, online cataloging.',
        'image': 'http://www.oclc.org/common/images/masterpage/mp-masthead-logo-oclc-en.gif',
        'link': 'http://www.oclc.org/about/history/beginning.htm'
        },
		{'start': '1968',
        'title': 'MARC is developed at Library of Congress (by Henriette Arram)'
        },			
		{'start': '1969',
        'title': 'Internet (Arpanet) first tested.'
        },	
		{'start': '1969',
        'title': 'GML (Generalized Markup Laguage) and Document Type Definition developed by Charles Goldfarb at IBM.'
        },	
		{'start': '1969',
        'title': 'CICS becomes available for IBM Mainframes',
        'description': 'Customer Information Control System (CICS) is a transaction server that runs primarily on IBM mainframe systems under z/OS and z/VSE.',
		'caption': 'Customer Information Control System (CICS) is a transaction server that runs primarily on IBM mainframe systems under z/OS and z/VSE.',
        'link': 'http://en.wikipedia.org/wiki/CICS'
        },
		{'start': '1970',
        'title': 'Dennis Ritchie and Kenneth Thompson finish writing the Unix operation system for a DEC minicomputer at Bell Labs',
        'description': 'Programmers Dennis Ritchie and Kenneth Thompson at Bell Labs complete the UNIX operating system on a spare DEC minicomputer. UNIX combines many of the time-sharing and file-management features offered by Multics and wins a wide following, particularly among scientists.',
		'caption': 'Programmers Dennis Ritchie and Kenneth Thompson at Bell Labs complete the UNIX operating system on a spare DEC minicomputer. UNIX combines many of the time-sharing and file-management features offered by Multics and wins a wide following, particularly among scientists.',
        'image': 'http://www.computerhistory.org/internet_history/thumbs/pdp-10_t.jpg',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_70s.html'
        },
		{'start': '1970',
        'title': 'Telnet protocol is completed.',
        'description': 'The Network Working Group completes the Telnet protocol and makes progress on the file transfer protocol (FTP) standard. At the end of the year, the ARPANET contains 19 nodes as planned.',
		'caption': 'The Network Working Group completes the Telnet protocol and makes progress on the file transfer protocol (FTP) standard. At the end of the year, the ARPANET contains 19 nodes as planned.',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_70s.html'
        },		
		{'start': '1971',
        'title': 'NOTIS (Northwestern Online Total Integrated System) programmed by Jim Aagaard at Northwestern University.',
        'description': 'NOTIS was a seminal integrated library system first created at Northwestern University, Evanston, IL USA in 1968. John P. McGowan, University Librarian from 1971 to 1992, recruited Professor James S. Aagaard to spearhead the project as lead programmer, and Velma Veneziano as systems analyst.',
		'caption': 'NOTIS was a seminal integrated library system first created at Northwestern University, Evanston, IL USA in 1968. John P. McGowan, University Librarian from 1971 to 1992, recruited Professor James S. Aagaard to spearhead the project as lead programmer, and Velma Veneziano as systems analyst.',
        'link': 'http://en.wikipedia.org/wiki/NOTIS'
        },
		{'start': '1971',
        'title': '    File Transfer Protocol is first published & used.'
        },
		{'start': '1971',
        'title': 'IBM offers TSO',
        'description': ' Time Sharing Option (TSO) is an interactive time-sharing environment for IBM mainframe operating systems, including OS/360 MVT, OS/VS2 (SVS), MVS, OS/390, and z/OS.',
		'caption': ' Time Sharing Option (TSO) is an interactive time-sharing environment for IBM mainframe operating systems, including OS/360 MVT, OS/VS2 (SVS), MVS, OS/390, and z/OS.',
        'link': 'http://www.tsotimes.com/articles/archive/spring04/TSO-Times-Spring04.pdf'
        },
		{'start': '1971',
        'title': 'Ray Tomlinson sends the first email ',
        'description': 'The first e-mail is sent. Ray Tomlinson of the research firm Bolt, Beranek and Newman sent the first e-mail when he was supposed to be working on a different project. Tomlinson, who is credited with being the one to decide on the "@" sign for use in e-mail, sent his message over a military network called ARPANET. When asked to describe the contents of the first email, Tomlinson said it was �something like "QWERTYUIOP"�',
		'caption': 'The first e-mail is sent. Ray Tomlinson of the research firm Bolt, Beranek and Newman sent the first e-mail when he was supposed to be working on a different project. Tomlinson, who is credited with being the one to decide on the "@" sign for use in e-mail, sent his message over a military network called ARPANET. When asked to describe the contents of the first email, Tomlinson said it was �something like "QWERTYUIOP"�',
        'image': 'http://www.computerhistory.org/timeline/images/1971_tomlinson1.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1971'
        },		
		{'start': '1971',
        'title': 'Project Gutenberg is started at the University of Illinois',
        'description': '<a href="http://www.gutenberg.org/wiki/Michael_S._Hart">Michael Hart</a> starts Project Gutenberg at the University of Illinois, which could arguably be the first digital libray.<br>Project Gutenberg (PG) is a volunteer effort to digitize and archive cultural works, to "encourage the creation and distribution of eBooks".[2] It was founded in 1971 by Michael S. Hart and is the oldest digital library.[3] Most of the items in its collection are the full texts of public domain books. The project tries to make these as free as possible, in long-lasting, open formats that can be used on almost any computer. As of July 2012, Project Gutenberg claimed over 40,000 items in its collection.',
		'caption': 'Project Gutenberg (PG) is a volunteer effort to digitize and archive cultural works, to "encourage the creation and distribution of eBooks".[2] It was founded in 1971 by Michael S. Hart and is the oldest digital library.[3] Most of the items in its collection are the full texts of public domain books. The project tries to make these as free as possible, in long-lasting, open formats that can be used on almost any computer. As of July 2012, Project Gutenberg claimed over 40,000 items in its collection.',
        'image': 'http://upload.wikimedia.org/wikipedia/en/5/54/Project_Gutenberg_logo.png',
        'link': 'http://www.gutenberg.org/'
        },	
		{'start': '1971',
        'title': 'MARC becomes an official standard. ',
        'link': 'http://www.loc.gov/loc/legacy/loc.html'
        },	
		{'start': '1973',
        'title': 'Vint Cerf and Bob Kahn propose TCP (Transmission Control Protocol) at  a meeting at the University of Sussix in the UK',
        'description': 'Kahn and Cerf set about designing a net-to-net connection protocol. Cerf leads the newly formed International Network Working Group. In September 1973, the two give their first paper on the new Transmission Control Protocol (TCP) at an INWG meeting at the University of Sussex in England. ',
		'caption': 'Kahn and Cerf set about designing a net-to-net connection protocol. Cerf leads the newly formed International Network Working Group. In September 1973, the two give their first paper on the new Transmission Control Protocol (TCP) at an INWG meeting at the University of Sussex in England. ',
        'image': 'http://www.computerhistory.org/internet_history/thumbs/kahn_t.jpg',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_70s.html'
        },	
		{'start': '1974',
        'title': 'The first mouse is used in the Alto at Xerox Research PARC',
        'description': 'Researchers at the Xerox Palo Alto Research Center designed the Alto � the first work station with a built-in mouse for input. The Alto stored several files simultaneously in windows, offered menus and icons, and could link to a local area network. Although Xerox never sold the Alto commercially, it gave a number of them to universities. Engineers later incorporated its features into work stations and personal computers.',
		'caption': 'Researchers at the Xerox Palo Alto Research Center designed the Alto � the first work station with a built-in mouse for input. The Alto stored several files simultaneously in windows, offered menus and icons, and could link to a local area network. Although Xerox never sold the Alto commercially, it gave a number of them to universities. Engineers later incorporated its features into work stations and personal computers.',
        'image': 'http://www.computerhistory.org/timeline/images/1974_alto.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1974'
        },		
		{'start': '1976',
        'title': 'Computer scientists at Berlekey revise Unix to incorporate TCP/IP',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_70s.html'
        },			
		{'start': '1976',
        'title': 'Gary Kildal creates the CP-M operating system for 8-bit machines',
        'description': 'Gary Kildall developed CP/M, an operating system for personal computers. Widely adopted, CP/M made it possible for one version of a program to run on a variety of computers built around eight-bit microprocessors.',
		'caption': 'Gary Kildall developed CP/M, an operating system for personal computers. Widely adopted, CP/M made it possible for one version of a program to run on a variety of computers built around eight-bit microprocessors.',
        'image': 'http://www.computerhistory.org/timeline/images/1976_cpm.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1976'
        },
		{'start': '1976',
        'title': 'Apple I is completed.',
        'description': 'Steve Wozniak designed the Apple I, a single-board computer. With specifications in hand and an order for 100 machines at $500 each from the Byte Shop, he and Steve Jobs got their start in business. In this photograph of the Apple I board, the upper two rows are a video terminal and the lower two rows are the computer. The 6502 microprocessor in the white package sits on the lower right. About 200 of the machines sold before the company announced the Apple II as a complete computer.',
		'caption': 'Steve Wozniak designed the Apple I, a single-board computer. With specifications in hand and an order for 100 machines at $500 each from the Byte Shop, he and Steve Jobs got their start in business. In this photograph of the Apple I board, the upper two rows are a video terminal and the lower two rows are the computer. The 6502 microprocessor in the white package sits on the lower right. About 200 of the machines sold before the company announced the Apple II as a complete computer.',
        'image': 'http://www.computerhistory.org/timeline/images/1976_apple.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1976'
        },
		{'start': '1977',
        'title': 'Apple II, Tandy TRS, and Commodore Pet home computers are sold',
        'description': 'Steve Wozniak and Steve Jobs announce the Apple II computer. Also introduced are the Tandy TRS-80 and the Commodore Pet. These three off-the-shelf machines create the consumer and small business markets for computers. ',
		'caption': 'Steve Wozniak and Steve Jobs announce the Apple II computer. Also introduced are the Tandy TRS-80 and the Commodore Pet. These three off-the-shelf machines create the consumer and small business markets for computers. ',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_70s.html'
        },			
		{'start': '1978',
        'title': '5.25 inch floppy drives became available.',
        'description': 'The 5 1/4" flexible disk drive and diskette were introduced by Shugart Associates in 1976. This was the result of a request by Wang Laboratories to produce a disk drive small enough to use with a desktop computer, since 8" floppy drives were considered too large for that purpose. By 1978, more than 10 manufacturers were producing 5 1/4" floppy drives.',
		'caption': 'The 5 1/4" flexible disk drive and diskette were introduced by Shugart Associates in 1976. This was the result of a request by Wang Laboratories to produce a disk drive small enough to use with a desktop computer, since 8" floppy drives were considered too large for that purpose. By 1978, more than 10 manufacturers were producing 5 1/4" floppy drives.',
        'image': 'http://www.computerhistory.org/timeline/images/1978_SA400.jpg',
        'link': 'http://www.computerhistory.org/timeline/?year=1978'
        },		
		{'start': '1981',
        'title': 'The Osborne I (11 Kg / 24 lbs) is sold. Also the IBM PC',
        'description': 'The first �portable� computer is launched in the form of the Osborne, a 24-pound suitcase-sized device.<br >The IBM PC is launched in August 1981. ',
		'caption': 'The first �portable� computer is launched in the form of the Osborne, a 24-pound suitcase-sized device.<br >The IBM PC is launched in August 1981. ',
        'image': 'http://www.computerhistory.org/internet_history/thumbs/osborne_t.jpg',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_80s.html'
        },		
		{'start': '1981',
        'title': 'MS-DOS is the software for the IBM PC',
        'description': 'IBM introduced its PC, igniting a fast growth of the personal computer market. The first PC ran on a 4.77 MHz Intel 8088 microprocessor and used Microsoft�s MS-DOS operating system.',
		'caption': 'IBM introduced its PC, igniting a fast growth of the personal computer market. The first PC ran on a 4.77 MHz Intel 8088 microprocessor and used Microsoft�s MS-DOS operating system.',
        'link': 'http://www.computerhistory.org/timeline/?year=1981'
        },			
		{'start': '1983',
        'title': 'MS-Word is announced.',
        'description': 'Microsoft announced Word, originally called Multi-Tool Word, and Windows. The latter doesn�t ship until 1985, although the company said it would be on track for an April 1984 release. In a marketing blitz, Microsoft distributed 450,000 disks demonstrating its Word program in the November issue of PC World magazine.',
		'caption': 'Microsoft announced Word, originally called Multi-Tool Word, and Windows. The latter doesn�t ship until 1985, although the company said it would be on track for an April 1984 release. In a marketing blitz, Microsoft distributed 450,000 disks demonstrating its Word program in the November issue of PC World magazine.',
        'link': 'http://www.computerhistory.org/timeline/?year=1983'
        },		
		{'start': '1984',
        'title': 'Macintosh computer is for sale.',
        'description': 'Apple announces the Macintosh. Its user-friendly interface swells the ranks of new computer users. ',
		'caption': 'Apple announces the Macintosh. Its user-friendly interface swells the ranks of new computer users. ',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_80s.html'
        },
		{'start': '1984',
        'title': 'Domain Name Service is announced',
        'description': 'Jon Postel and Paul Mockapetris of USC/ISI and Craig Partridge of BBN develop the Domain Name System (DNS) and recommend the use of the now familiar user@host.domain addressing system.',
		'caption': 'Jon Postel and Paul Mockapetris of USC/ISI and Craig Partridge of BBN develop the Domain Name System (DNS) and recommend the use of the now familiar user@host.domain addressing system.',
        'image': 'http://www.computerhistory.org/internet_history/thumbs/1983_topo_t.gif',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_80s.html'
        },		
		{'start': '1984',
        'title': 'The British JANET network focuses on educational institutions.',
        'description': 'The British JANET explicitly announces its intention to serve the nation�s higher education community, regardless of discipline.',
		'caption': 'The British JANET explicitly announces its intention to serve the nation�s higher education community, regardless of discipline.',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_80s.html'
        },		
		{'start': '1985',
        'title': 'NSF establishes supercomputing centers at Cornell, Princeton, UIUC, Pittsburg, San Diego',
        'description': 'NSF announces the award of five supercomputing center contracts',
		'caption': 'NSF announces the award of five supercomputing center contracts',
        'image': 'http://upload.wikimedia.org/wikipedia/en/e/ec/Nsfnet_logo.jpg',
        'link': 'http://www.nsf.gov/about/history/nsf0050/internet/launch.htm'
        },		
		{'start': '1985',
		'end': '1987',
        'title': 'Project Perseus',
        'description': '"Perseus is a practical experiment in which we explore possibilities and challenges of digital collections in a networked world. "',
		'caption': '"Perseus is a practical experiment in which we explore possibilities and challenges of digital collections in a networked world. "',
        'image': 'http://www.perseus.tufts.edu/img/newbanner.png',
        'link': 'http://dl.acm.org/citation.cfm?doid=226931.226932',
        'isDuration' : true,
        'icon' : "dark-red-circle.png",        
        'color' : 'red',
        'textColor' : 'green'	
        },
		{'start': '1986',
        'title': 'NSFNet in the US opened',
		'image': 'http://upload.wikimedia.org/wikipedia/en/e/ec/Nsfnet_logo.jpg',
        'link': 'http://www.nsf.gov/about/history/nsf0050/internet/launch.htm'
        },	
		{'start': '1986',
        'title': 'SGML becomes ISO 8879:1986'
        },
		{'start': '1986',
        'title': 'Uncle Ezra begins on CUINFO at Cornell',
        'description': '"Dear Uncle Ezra" was the first on-line helpline in the world. One of the first queries, in the fall of 1986, was from a dining worker who was diagnosed with AIDS. As a very responsive communication service in a world increasingly filled with layers of bureaucracy before one can find a live, warm human being, Ezra has been able to help students, staff, alumni, prospective students, parents -- readers all around the world -- calm their fears, consider courses of action, determine resources, feel heard and feel empowered.',
		'caption': '"Dear Uncle Ezra" was the first on-line helpline in the world. One of the first queries, in the fall of 1986, was from a dining worker who was diagnosed with AIDS. As a very responsive communication service in a world increasingly filled with layers of bureaucracy before one can find a live, warm human being, Ezra has been able to help students, staff, alumni, prospective students, parents -- readers all around the world -- calm their fears, consider courses of action, determine resources, feel heard and feel empowered.',
        'image': 'http://ezra.cornell.edu/images/header_ezra.jpg',
        'link': 'http://ezra.cornell.edu/history.php'
        },	
		{'start': '1987',
        'title': 'NSF Net implemets a t1 backbone',
        'description': 'Between the beginning of 1986 and the end of 1987 the number of networks grows from 2,000 to nearly 30,000.',
		'caption': 'Between the beginning of 1986 and the end of 1987 the number of networks grows from 2,000 to nearly 30,000.',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_80s.html'
        },
		{'start': '1987',
        'title': 'Apple Hypercard becomes available',
        'description': 'Apple engineer William Atkinson designed HyperCard, a software tool that simplifies development of in-house applications. HyperCard differed from previous programs of its sort because Atkinson made it interactive rather than language-based and geared it toward the construction of user interfaces rather than the processing of data. In HyperCard, programmers built stacks with the concept of hypertext links between stacks of pages. Apple distributed the program free with Macintosh computers until 1992.<br />Hypercard users could look through existing HyperCard stacks as well as add to or edit the stacks. As a stack author, a programmer employed various tools to create his own stacks, linked together as a sort of slide show. At the lowest level, the program linked cards sequentially in chronological ordered, but the HyperTalk programming language allowed more sophisticated links.',
		'caption': 'Apple engineer William Atkinson designed HyperCard, a software tool that simplifies development of in-house applications. HyperCard differed from previous programs of its sort because Atkinson made it interactive rather than language-based and geared it toward the construction of user interfaces rather than the processing of data. In HyperCard, programmers built stacks with the concept of hypertext links between stacks of pages. Apple distributed the program free with Macintosh computers until 1992.',
		'link': 'http://www.computerhistory.org/internet_history/internet_history_80s.html'
        },		
		{'start': '1987',
        'title': 'Electronic theses and dissertations discussed in Ann Arbor, MI',
        'description': 'The concept of electronic theses and dissertations (ETDs) was first discussed at a 1987 meeting in Ann Arbor, Michigan, organized by UMI and attended by representatives from Virginia Tech, the University of Michigan, and two small software companies�Toronto-based SoftQuad and Michigan-based ArborText. ',
		'caption': 'The concept of electronic theses and dissertations (ETDs) was first discussed at a 1987 meeting in Ann Arbor, Michigan, organized by UMI and attended by representatives from Virginia Tech, the University of Michigan, and two small software companies�Toronto-based SoftQuad and Michigan-based ArborText. ',
		'image': 'http://www.ndltd.org/logo.gif',
        'link': 'http://www.ndltd.org/about/history'
        },
		{'start': '1988',
        'title': 'Morris WORM infects 6000 computers',
        'description': 'The Morris WORM burrows on the Internet into 6,000 of the 60,000 hosts now on the network. This is the first worm experience and DARPA forms the Computer Emergency Response Team (CERT) to deal with future such incidents.',
		'caption': 'The Morris WORM burrows on the Internet into 6,000 of the 60,000 hosts now on the network. This is the first worm experience and DARPA forms the Computer Emergency Response Team (CERT) to deal with future such incidents.',
        'link': 'http://www.computerhistory.org/internet_history/internet_history_80s.html'
        },		
      
]
}
